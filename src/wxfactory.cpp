// (c) 2015 - 2017: Marcel Admiraal

#include "wxfactory.h"

#include <wx/image.h>
#include <wx/font.h>

#include <iostream>

namespace ma
{
    wxBitmap rgbaTowxBitmap(const unsigned int width, const unsigned int height,
            const unsigned char* rgba)
    {
        unsigned char* data = (unsigned char*)malloc(width * height * 3);
        unsigned char* alpha = (unsigned char*)malloc(width * height);
        for (unsigned int row = 0; row < height; row++)
        {
            for (unsigned int col = 0; col < width; col++)
            {
                for (unsigned int rgb = 0; rgb < 3; rgb++)
                {
                    data[(row * width + col) * 3 + rgb] =
                        rgba[(row * width + col) * 4 + rgb];
                }
                alpha[row * width + col] = rgba[(row * width + col) * 4 + 3];
            }
        }
        wxImage image(width, height, data, alpha);
        wxBitmap bitmap(image);
        return bitmap;
    }

    void getColours(wxColour colourArray[], const unsigned int colours,
            const bool darkBack)
    {
        for (unsigned int colour = 0; colour < colours; colour++)
        {
            unsigned int colourCode = colour % 19;
            unsigned char greenCode;
            unsigned char redCode;
            unsigned char blueCode;
            if (colourCode < 7)
            {
                ++colourCode;
                greenCode = (colourCode & 1) * 255;
                redCode = ((colourCode & 2) >> 1) * 255;
                blueCode = ((colourCode & 4) >> 2) * 255;
                if (colourCode == 7 && !darkBack)
                {
                    greenCode = 0;
                    redCode = 0;
                    blueCode = 0;
                }
            }
            else
            {
                colourCode -= 7;
                unsigned int channelLock = (colourCode % 12) / 4;
                unsigned int fixLevel = 127;
                colourCode = (colourCode % 4);
                switch (channelLock)
                {
                    case 0:
                        greenCode = fixLevel;
                        redCode = (colourCode & 1) * 255;
                        blueCode = ((colourCode & 2) >> 1) * 255;
                        break;
                    case 1:
                        greenCode = (colourCode & 1) * 255;
                        redCode = fixLevel;
                        blueCode = ((colourCode & 2) >> 1) * 255;
                        break;
                    case 2:
                        greenCode = (colourCode & 1) * 255;
                        redCode = ((colourCode & 2) >> 1) * 255;
                        blueCode = fixLevel;
                        break;
                }
                if (colourCode == 0 && darkBack)
                {
                    greenCode += 32;
                    redCode += 32;
                    blueCode += 32;
                }
                if (colourCode == 3 && !darkBack)
                {
                    greenCode -= 32;
                    redCode -= 32;
                    blueCode -= 32;
                }
            }
            colourArray[colour] = wxColour(redCode, greenCode, blueCode);
        }
    }
}
