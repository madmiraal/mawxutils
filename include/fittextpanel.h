// A wxPanel to display text that is sized to fit the panel.
//
// (c) 2016: Marcel Admiraal

#ifndef FITTEXTPANEL_H
#define FITTEXTPANEL_H

#include <wx/panel.h>

namespace ma
{
    class FitTextPanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the parent window.
         * @param text      The initial text to display.
         */
        FitTextPanel(wxWindow* parent, const wxString& text = wxEmptyString,
            const wxColour& textColour = *wxWHITE,
            const wxColour& backgroundColour = *wxBLACK);

        void setText(const wxString& text);

        void setTextColour(const wxColor& textColour);

        void setBackgroundColour(const wxColor& backgroundColour);

    private:
        void stopErase(wxEraseEvent& event);
        void paint(wxPaintEvent& event);

        wxString text;
        wxColour textColour;
        wxColour backgroundColour;
    };

    /**
     * Returns the point size of the windows's font that best fits a string in a
     * box of the given width and height in the window object.
     *
     * @param window    Pointer to the window with the font that the text to be
     *                  fitted will be displayed in.
     * @param text      The text to best fit.
     * @param width     The width of the box to best fit.
     * @param height    The height of the box to best fit.
     * @return          The font point size.
     */
    unsigned int getBestFitFontSize(const wxWindow* window, const wxString& text,
            const unsigned int width, const unsigned int height);
}

#endif // FITTEXTPANEL_H
