// (c) 2016: Marcel Admiraal

#include "control.h"

namespace ma
{
    Control::Control() :
        // Initialise continuous loop control variables.
        running(true), paused(true)
    {
        // Start the loop thread.
        CreateThread();
        GetThread()->Run();

        Bind(wxEVT_TIMER, &Control::timerEvent, this);
    }

    Control::~Control()
    {
        Unbind(wxEVT_TIMER, &Control::timerEvent, this);
        for (Iterator<wxTimer*> iter = timerList.first(); iter.valid(); ++iter)
        {
            delete (*iter);
        }

        // Stop the loop thread.
        running = false;
        GetThread()->Wait();
    }

    bool Control::isLoopPaused()
    {
        return paused;
    }

    void Control::pauseLoop(bool pause)
    {
        paused = pause;
    }

    void Control::startTimer(const unsigned int id, const unsigned int period)
    {
        wxTimer* timer = 0;
        for (Iterator<wxTimer*> iter = timerList.first(); iter.valid(); ++iter)
        {
            wxTimer* thisTimer = *iter;
            if (thisTimer->GetId() == (int)id) timer = thisTimer;
        }
        if (timer == 0)
        {
            timer = new wxTimer(this, id);
            timerList.append(timer);
        }
        timer->Start(period);
    }

    void Control::stopTimer(const unsigned int id)
    {
        wxTimer* timer = 0;
        for (Iterator<wxTimer*> iter = timerList.first(); iter.valid(); ++iter)
        {
            wxTimer* thisTimer = *iter;
            if (thisTimer->GetId() == (int)id) timer = thisTimer;
        }
        if (timer != 0) timer->Stop();
    }

    unsigned int Control::getTimerPeriod(const unsigned int id)
    {
        wxTimer* timer = 0;
        for (Iterator<wxTimer*> iter = timerList.first(); iter.valid(); ++iter)
        {
            wxTimer* thisTimer = *iter;
            if (thisTimer->GetId() == (int)id) timer = thisTimer;
        }
        if (timer != 0) return timer->GetInterval();
        return -1;
    }

    Str& Control::getStatusMessage()
    {
        return statusMessage;
    }

    void Control::setStatusMessage(const Str& message)
    {
        statusMessage = message;
    }

    wxThread::ExitCode Control::Entry()
    {
        while (running)
        {
            if (!paused)
            {
                loopFunction();
            }
            // When compiling under windows, Yield() is defined in winbase.h,
            // which is included by windows.h, which is included in serial.h.
            // This deletes any Yield() and causes the syntax error "expected
            // unqualified-id before ';'.
            // Yield() was used by 16-bit multi-tasking applications that were
            // required to voluntarily relinquish control. Since multi-threaded
            // operating systems don't require this, Microsoft replaced it with
            // an empty macro in attempt to achieve compile-level code
            // compatibility.
            // Yield is used here, because the default loop function does
            // nothing.
            #ifdef Yield
            #undef Yield
            #endif // Yield
            wxThread::Yield();
        }
        return (wxThread::ExitCode)0;
    }

    void Control::timerEvent(wxTimerEvent& event)
    {
        timerFunction(event.GetId());
    }
}
