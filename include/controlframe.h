// A generic control frame class.
//
// Enables the control class and includes a menubar, a toolbar and a statusbar
// that can be updated outside the frame's thread.
//
// (c) 2016: Marcel Admiraal

#ifndef CONTROLFRAME_H
#define CONTROLFRAME_H

#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/toolbar.h>
#include <wx/statusbr.h>

#include "control.h"
#include "str.h"

namespace ma
{
    class ControlFrame : public wxFrame
    {
    public:
        /**
         * Default constructor. All parameters are optional.
         *
         * @param parent    Pointer to the parent window.
         * @param id        The frame's id.
         * @param title     The text displayed at the top of the window.
         * @param position  A wxPoint specifying the location of the window.
         * @param size      A wxSize specifying the size of the window.
         * @param syle      The combination of wxstyle codes.
         * @param name      A name for the window.
         */
        ControlFrame(
            wxWindow* parent = 0L,
            const wxWindowID id = wxID_ANY,
            const Str& title = Str(),
            const wxPoint& position = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            const long style = wxDEFAULT_FRAME_STYLE,
            const wxString& name = wxFrameNameStr);

        /**
         * Control constructor.
         *
         * @param control   The controller.
         * @param title     The text displayed at the top of the window.
         * @param position  A wxPoint identifying the location of the window.
         * @param size      A wxSize identifying the size of the window.
         * @param syle      The combination of wxstyle codes.
         * @param parent    The parent of the window, if any.
         * @param id        The window's id.
         * @param name      A name for the window.
         */
        ControlFrame(
            Control* control,
            wxWindow* parent = 0,
            const wxWindowID id = wxID_ANY,
            const Str& title = Str(),
            const wxPoint& position = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            const long style = wxDEFAULT_FRAME_STYLE,
            const wxString& name = wxFrameNameStr);

        /**
         * Destructor.
         */
        virtual ~ControlFrame();

        /**
         * Thread safe status bar update.
         *
         * @param status    The new status bar text.
         */
        void updateStatusBar(const Str& status);

    protected:
        Control* control;

        wxMenuBar* menuBar;
        wxToolBar* toolBar;
        wxStatusBar* statusBar;

    private:
        // Event functions.
        void onIdle(wxIdleEvent& evt);
        void onClose(wxCloseEvent& event);
    };
}

#endif // CONTROLFRAME_H
