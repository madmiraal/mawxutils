// (c) 2015: Marcel Admiraal

#include "serialpanel.h"

#include <wx/sizer.h>
#include <wx/stattext.h>

namespace ma
{
    SerialPanel::SerialPanel(wxWindow* parent, Serial* serial) :
        wxPanel(parent), serial(serial)
    {
        wxBoxSizer* serialPanelSizer = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* portTextLabel = new wxStaticText(this, wxID_ANY,
                wxT("Port Name:"));
        serialPanelSizer->Add(portTextLabel, 0, wxTOP | wxBOTTOM, 10);
        portTextCtrl = new wxTextCtrl(this, wxID_ANY);
        serialPanelSizer->Add(portTextCtrl, 0, wxTOP | wxBOTTOM, 5);
        wxButton* connectPortButton = new wxButton(this, wxID_ANY,
                wxT("Connect"));
        serialPanelSizer->Add(connectPortButton, 0, wxALL, 5);
        connectedLight = new wxStaticBitmap(this, wxID_ANY, blueLight);
        serialPanelSizer->Add(connectedLight);
        SetSizer(serialPanelSizer);

        *portTextCtrl << getPortName();
        setLight();

        portTextCtrl->Bind(wxEVT_COMMAND_TEXT_UPDATED,
                &SerialPanel::portNameChanged, this);
        connectPortButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &SerialPanel::connectPort, this);
    }

    SerialPanel::~SerialPanel()
    {
    }

    wxString SerialPanel::getStatus()
    {
        return status;
    }

    wxString SerialPanel::getPortName()
    {
        return wxString(serial->getPortString().c_str());
    }

    void SerialPanel::setSerial(Serial* serial)
    {
        this->serial = serial;
        portTextCtrl->SetValue(getPortName());
        setLight();
    }

    void SerialPanel::portNameChanged(wxCommandEvent & event)
    {
        connectedLight->SetBitmap(blueLight);
    }

    void SerialPanel::connectPort(wxCommandEvent &event)
    {
        serial->setPort(Str(portTextCtrl->GetLineText(0).mb_str()));
        setLight();
    }

    void SerialPanel::setLight()
    {
        if (serial->isConnected())
        {
            connectedLight->SetBitmap(greenLight);
            status = wxT("Connected to ") + getPortName();
        }
        else
        {
            connectedLight->SetBitmap(redLight);
            status = wxT("Failed to connect to ") + getPortName();
        }
    }
}
