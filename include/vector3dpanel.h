// A wxPanel to display a 3D vector.
//
// (c) 2014 - 2017: Marcel Admiraal

#ifndef VECTOR3DPANEL_H
#define VECTOR3DPANEL_H

#include <wx/panel.h>
#include <wx/timer.h>
#include <wx/thread.h>

#include "quaternion.h"
#include "vector3d.h"

namespace ma
{
    class Vector3DPanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the parent window.
         * @param maxLength The maximum length of the vector.
         * @param useLeft   Whether or not to use the left hand coordinate
         *                  system.
         *                  The default is to use the right hand coordinate
         *                  system.
         */
        Vector3DPanel(wxWindow* parent, double maxLength = 1,
                bool useLeft = false);

        /**
         * Default destructor.
         */
        virtual ~Vector3DPanel();

        /**
         * Sets the vector to display.
         *
         * @param vector The 3D vector.
         */
        void setVector(const ma::Vector3D& vector);

        /**
         * Sets the vector to display.
         *
         * @param vector A three dimensional array describing the vector.
         */
        void setVector(const double* vector);

    private:
        double maxLength;
        bool useLeft;
        wxMutex dataLock;
        ma::Vector3D vector;
        int xCentre, yCentre;
        double scale;

        // Erase event function.
        void stopErase(wxEraseEvent& event);
        // Paint event function.
        void paint(wxPaintEvent& event);
        // Idle event function.
        void onIdle(wxIdleEvent& evt);
    };
}
#endif // VECTOR3DPANEL_H
