# MAwxUtils #

Cross-platform C++ code for simple utitilites to be used with wxWidgets:

## Control Utilities ##
* ma::Control: A generic control class that runs inside a ControlFrame and includes a continuously looping thread that can be used as the primary thread, and any number of regularly triggered timers.
* ma::Logger: A generic logger that logs any data with a timestamp relative to the start of the log file.
* ma::LogPlayback: A generic playback class that replays a logfile created with the logger with the same time gaps.

## Display Utilities ##
* ma::ControlFrame: A genric frame class that enables the control class and includes a menubar, a toolbar and a status bar that can be updated outside the frame's thread.
* ma::SericalPanel: A wxPanel for connecting to MAUtils ma::Serial ports.
* ma::OscilloscopePanel: A wxPanel for displaying multiple data streams in real-time.
* ma::FitTextPanel: A wxPanel that automatically resizes text to fit inside the panel.
* ma::CuboidPanel: A wxPanel for displaying a rotatable 3-dimensional cube.
* ma::Display3DPanel: A wxPanel for displaying a 3-dimensional vector.

## Other Utilities ##
### wxFactory ##
* ma::rgbaToWxBitmap: Converts a RGBA character array to a wxBitmap.
* ma::getColours: Creates an array of the required number of different colours.

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
