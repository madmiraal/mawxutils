// A wxPanel to display a 3D cube.
//
// (c) 2014: Marcel Admiraal

#ifndef CUBOIDPANEL_H
#define CUBOIDPANEL_H

#include <wx/panel.h>
#include <wx/timer.h>
#include <wx/thread.h>

#include "quaternion.h"

namespace ma
{
    class CuboidPanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the parent window.
         * @param useLeft   Whether or not to use the left hand coordinate
         *                  system.
         *                  The default is to use the right hand coordinate
         *                  system.
         */
        CuboidPanel(wxWindow* parent, bool useLeft = false);

        /**
         * Default destructor.
         */
        virtual ~CuboidPanel();

        /**
         * Sets the orientation of the cube.
         *
         * @param orientation The rotation quaternion describing
         *                    the orientation of the cube.
         */
        void setOrientation(const Quaternion& orientation);

    private:
        Quaternion orientation;
        wxMutex dataLock;
        bool useLeft;

        // Initialise
        void initialise();
        // Erase event function.
        void stopErase(wxEraseEvent& event);
        // Paint event function.
        void paint(wxPaintEvent& event);
        // Idle event function.
        void onIdle(wxIdleEvent& evt);
    };
}

#endif // CUBOIDPANEL_H
