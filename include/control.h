// A generic control class.
//
// Includes a continuously looping thread and any number of regularly triggered
// timers.
//
// (c) 2016: Marcel Admiraal

#ifndef CONTROL_H
#define CONTROL_H

#include <wx/event.h>
#include <wx/thread.h>
#include <wx/timer.h>

#include "str.h"
#include "list.h"

namespace ma
{
    class Control : public wxEvtHandler, public wxThreadHelper
    {
    public:
        /**
         * Default Constructor.
         */
        Control();

        /**
         * Destructor.
         */
        virtual ~Control();

        /**
         * Returns whether or not the loop function is paused.
         *
         * @return Whether or not the loop function is paused.
         */
        bool isLoopPaused();

        /**
         * Pause or unpause the loop function.
         *
         * @param pause Whether to pause (true) or unpause (false) the loop.
         */
        void pauseLoop(bool pause = true);

        /**
         * Starts the specified regular function timer.
         *
         * @param id        The id of the timer.
         * @param period    The timer period in milliseconds.
         */
        void startTimer(const unsigned int id, const unsigned int period = -1);

        /**
         * Stops the specified regular function timer.
         */
        void stopTimer(const unsigned int id);

        /**
         * Returns the specified regular function timer period.
         *
         * @return The specified regular function timer period.
         */
        unsigned int getTimerPeriod(const unsigned int id);

        /**
         * Returns the current status message.
         *
         * @return The current status message.
         */
        Str& getStatusMessage();

        /**
         * Sets the status message.
         *
         * @param message The new status message.
         */
        void setStatusMessage(const Str& message);

    private:
        // Override the loop function to implement a continuous function call.
        virtual void loopFunction() {}
        // Override the timer function to implement the regular function calls.
        virtual void timerFunction(const unsigned int id) {}

        // wxThreadHelper entry function.
        virtual wxThread::ExitCode Entry();
        // Timer event.
        void timerEvent(wxTimerEvent& event);

        // Continuous loop control variables.
        bool running;
        bool paused;
        // Timers.
        List<wxTimer*> timerList;

        Str statusMessage;
    };
}

#endif // CONTROL_H
