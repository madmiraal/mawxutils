// (c) 2016 - 2017: Marcel Admiraal

#include "vector3dframe.h"

#include <wx/sizer.h>

namespace ma
{
    Vector3DFrame::Vector3DFrame(const double maxLength, wxWindow* parent,
            wxWindowID id, const wxString& title, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name) :
        wxFrame(parent, id, title, pos, size, style, name)
    {
        wxBoxSizer* frameSizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* valueSizer = new wxBoxSizer(wxHORIZONTAL);
        vectorText = new wxStaticText(this, wxID_ANY, wxT("Vector: "));
        magnitudeText = new wxStaticText(this, wxID_ANY, wxT("Magnitude: "));
        valueSizer->Add(vectorText, 1, wxEXPAND);
        valueSizer->Add(magnitudeText, 1, wxEXPAND);
        frameSizer->Add(valueSizer, 0, wxEXPAND);

        vectorPanel = new ma::Vector3DPanel(this, maxLength);
        frameSizer->Add(vectorPanel, 1, wxEXPAND);
        SetSizer(frameSizer);

        time(&lastRefresh);
    }

    Vector3DFrame::~Vector3DFrame()
    {
    }

    void Vector3DFrame::setVector(const ma::Vector3D& newVector)
    {
        vectorPanel->setVector(newVector);
        time_t now;
        if (difftime(time(&now), lastRefresh) >= 1)
        {
            lastRefresh = now;
            wxString vectorString("Vector: ");
            vectorString << "[" << newVector[0] << ", " << newVector[1] << ", "
                    << newVector[2] << "]";
            vectorText->GetEventHandler()->CallAfter(
                    &wxStaticText::SetLabel, vectorString);
            wxString magnitudeString("Magnitude: ");
            magnitudeString << std::sqrt(newVector.elementSquaredSum());
            magnitudeText->GetEventHandler()->CallAfter(
                    &wxStaticText::SetLabel, magnitudeString);
        }
    }
}
