// A wxDialog for editing simple text files.
//
// (c) 2017: Marcel Admiraal

#ifndef TEXTEDITORDIALOG_H
#define TEXTEDITORDIALOG_H

#include <wx/dialog.h>
#include <wx/textctrl.h>

#include "str.h"

namespace ma
{
    class TextEditorDialog : public wxDialog
    {
    public:
        /**
         * Constructor
         *
         * @param parent        Pointer to the parent window.
         * @param id            The frame's id.
         * @param title         The text displayed at the top of the window.
         * @param initialText   The initial text to display in the editor.
         * @param position      A wxPoint specifying the location of the window.
         * @param size          A wxSize specifying the size of the window.
         * @param syle          The combination of wxstyle codes.
         * @param name          A name for the window.
         */
        TextEditorDialog(wxWindow* parent = 0L, wxWindowID = wxID_ANY,
                const Str& title = Str(), const Str& initialText = Str(),
                const wxPoint& position = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxDEFAULT_DIALOG_STYLE,
                const wxString& name = wxDialogNameStr);

        unsigned int getLines() const;

        Str getLine(const unsigned int lineNumber) const;

        Str getText() const;

    private:
        void maxLengthReached(wxCommandEvent& event);

        wxTextCtrl* textCtrl;
    };

    enum {
        ID_TEXT
    };
}

#endif // TEXTEDITORDIALOG_H
