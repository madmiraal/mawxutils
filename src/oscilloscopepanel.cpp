// (c) 2014 - 2017: Marcel Admiraal

#include "oscilloscopepanel.h"

#include <wx/dcbuffer.h>

#include "wxfactory.h"

#define PIXELS 1000

namespace ma
{
    OscilloscopePanel::OscilloscopePanel(wxWindow* parent,
            const unsigned int channels, const unsigned int displayPeriod,
            const double dataMin, const double dataMax,
            const unsigned int markerPeriod, const bool darkBack) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxFULL_REPAINT_ON_RESIZE | wxSUNKEN_BORDER),
        data(channels, 1000 * PIXELS / displayPeriod, displayPeriod),
        dataMin(dataMin), dataMax(dataMax), displayPeriod(displayPeriod),
        markerPeriod(markerPeriod), darkBack(darkBack), frozen(false)
    {
        colour = new wxColour[channels];
        getColours(colour, channels, darkBack);

        Bind(wxEVT_ERASE_BACKGROUND, &OscilloscopePanel::stopErase, this);
        Bind(wxEVT_PAINT, &OscilloscopePanel::paint, this);
        Bind(wxEVT_IDLE, &OscilloscopePanel::onIdle, this);
    }

    OscilloscopePanel::~OscilloscopePanel()
    {
        delete[] colour;
    }

    void OscilloscopePanel::addData(const double value,
            const unsigned int channel)
    {
        data.addData(value, channel);
    }

    void OscilloscopePanel::addData(const Vector& value)
    {
        data.addData(value);
    }

    void OscilloscopePanel::setColour(const wxColour& newColour,
            const unsigned int channel)
    {
        colour[channel] = newColour;
    }

    void OscilloscopePanel::setColour(const wxColour newColour[])
    {
        unsigned int channels = data.getChannels();
        for (unsigned int channel = 0; channel < channels; ++channel)
            setColour(newColour[channel], channel);
    }

    void OscilloscopePanel::freezeOutput(const bool freeze)
    {
        frozen = freeze;
    }

    void OscilloscopePanel::stopErase(wxEraseEvent& event)
    {
        // Intercepting erase event to stop erasing.
    }

    void OscilloscopePanel::paint(wxPaintEvent& event)
    {
        // Get panel dimensions.
        wxSize windowSize = this->GetSize();
        int height = windowSize.GetHeight();
        int width = windowSize.GetWidth();
        unsigned int channels = data.getChannels();

        // Calculate scaling factors.
        double yScale = height / (dataMax - dataMin);
        double xScale = 1.0 * width / PIXELS;

        // Create buffered paint event device context.
        wxBitmap buffer(windowSize);
        wxBufferedPaintDC dc(this, buffer);
        if (darkBack)
            dc.SetBrush(*wxBLACK);
        else
            dc.SetBrush(*wxWHITE);
        dc.DrawRectangle(0, 0, width, height);

        // Draw axes.
        if (darkBack)
            dc.SetPen(wxPen(wxColour(255,191,0)));
        else
            dc.SetPen(wxPen(wxColour(191,127,31)));
        // Horizontal axis.
        int y0 = height + (dataMin * yScale);
        dc.DrawLine(0, y0, width, y0);
        // Vertical markers.
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        unsigned int markers = displayPeriod / markerPeriod;
        unsigned int first = now.count() % markerPeriod;
        for (unsigned int mark = 0; mark < markers; ++mark)
        {
            unsigned int current = first + markerPeriod * mark;
            int x = width - xScale * PIXELS * current / displayPeriod;
            dc.DrawLine(x, 0, x, height);
        }

        // Draw data lines.
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            dc.SetPen(colour[channel]);
            Vector currentData = data.getData(channel);
            for (unsigned int i = 1; i < PIXELS; ++i)
            {
                dc.DrawLine(
                        (i - 1) * xScale,
                        height - ((currentData[i-1] - dataMin) * yScale),
                        i * xScale,
                        height - ((currentData[i] - dataMin) * yScale)
                        );
            }
        }
    }

    void OscilloscopePanel::onIdle(wxIdleEvent& evt)
    {
        if (!frozen) Refresh();
    }
}
