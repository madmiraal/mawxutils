// (c) 2017: Marcel Admiraal

#include "plot2dpanel.h"

#include <wx/dcbuffer.h>
#include <wx/colour.h>

#include "wxfactory.h"

#define PIXELS 1000

namespace ma
{
        Plot2DPanel::Plot2DPanel(wxWindow* parent, unsigned int channels,
                double xMin, double xMax, double yMin, double yMax,
                bool axes, bool connected) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxFULL_REPAINT_ON_RESIZE | wxSUNKEN_BORDER),
        channels(channels), dataPoints(0), yData(new Vector[channels]),
        xMin(xMin), xMax(xMax), yMin(yMin), yMax(yMax), axes(axes),
        connected(connected), colour(new wxColour[channels]), dataChanged(false)
    {
        getColours(colour, channels);

        Bind(wxEVT_ERASE_BACKGROUND, &Plot2DPanel::stopErase, this);
        Bind(wxEVT_PAINT, &Plot2DPanel::paint, this);
        Bind(wxEVT_IDLE, &Plot2DPanel::onIdle, this);
    }

    Plot2DPanel::~Plot2DPanel()
    {
        delete[] yData;
        delete[] colour;
    }

    void Plot2DPanel::setXData(const Vector& xData)
    {
        wxMutexLocker lock(dataLock);
        dataChanged = true;
        this->xData = xData;
        setDataPoints();
    }

    void Plot2DPanel::setYData(const Vector& yData, unsigned int channel)
    {
        wxMutexLocker lock(dataLock);
        dataChanged = true;
        this->yData[channel] = yData;
        setDataPoints();
    }

    void Plot2DPanel::setXRange(const double xMin, const double xMax)
    {
        wxMutexLocker lock(dataLock);
        dataChanged = true;
        this->xMin = xMin;
        this->xMax = xMax;
    }

    void Plot2DPanel::setYRange(const double yMin, const double yMax)
    {
        wxMutexLocker lock(dataLock);
        dataChanged = true;
        this->yMin = yMin;
        this->yMax = yMax;
    }

    void Plot2DPanel::setColour(wxColour newColour, unsigned int channel)
    {
        colour[channel] = newColour;
    }

    void Plot2DPanel::setColour(wxColour newColour[])
    {
        for (unsigned int channel = 0; channel < channels; ++channel)
            setColour(newColour[channel], channel);
    }

    void Plot2DPanel::displayAxes(bool axes)
    {
        this->axes = axes;
    }

    void Plot2DPanel::connectData(bool connected)
    {
        this->connected = connected;
    }

    void Plot2DPanel::setDataPoints()
    {
        dataPoints = UINT_MAX;
        if (xData.size() != 0) dataPoints = xData.size();
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            unsigned int ySize = yData[channel].size();
            if (ySize < dataPoints) dataPoints = ySize;
        }
    }

    void Plot2DPanel::drawX(wxDC& dc, double x, double y,
            double xScale, double yScale)
    {
        dc.DrawLine(x - xScale*2, y - yScale*2, x + xScale*2, y + yScale*2);
        dc.DrawLine(x + xScale*2, y - yScale*2, x - xScale*2, y + yScale*2);
    }

    void Plot2DPanel::stopErase(wxEraseEvent& event)
    {
        // Intercepting erase event to stop erasing.
    }

    void Plot2DPanel::paint(wxPaintEvent& event)
    {
        // Get panel dimensions.
        wxSize windowSize = this->GetSize();
        int width = windowSize.GetWidth();
        int height = windowSize.GetHeight();
        // Calculate scaling factors.
        double xScale = width / (xMax - xMin);
        double yScale = height / (yMax - yMin);
        // Create buffered paint event device context.
        wxBitmap buffer(windowSize);
        wxBufferedPaintDC dc(this, buffer);
        dc.SetBrush(*wxBLACK);
        dc.DrawRectangle(0, 0, width, height);

        wxMutexLocker lock(dataLock);
        dataChanged = false;
        // Draw axes.
        if (axes)
        {
            dc.SetPen(wxPen(wxColour(255,191,0)));
            int x0 = - xMin * xScale;
            int y0 = height + (yMin * yScale);
            if (x0 > 0 && x0 < width) dc.DrawLine(x0, 0, x0, height);
            if (y0 > 0 && y0 < height) dc.DrawLine(0, y0, width, y0);
        }

        bool hasX = xData.size() != 0;

        // Plot data.
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            dc.SetPen(colour[channel]);
            double x, y;
            double previousX, previousY;
            for (unsigned int point = 0; point < dataPoints; ++point)
            {
                // For connected points remember the previous point.
                if (connected && point > 0)
                {
                    previousX = x;
                    previousY = y;
                }

                // Determine the location of the points.
                if (hasX)
                {
                    x = (xData[point] - xMin) * xScale;
                    y = height - (yData[channel][point] - yMin) * yScale;
                }
                else
                {
                    x = point * xScale;
                    unsigned int index = point + xMin;
                    if (index >= 0 && index < dataPoints)
                    {
                        y = height - (yData[channel][index] - yMin) * yScale;
                    }
                }

                // Adjust y to fit.
                if (y < 0) y = -1;
                if (y > height) y = height;

                // Draw points.
                if (x >= 0 && x <= width)
                {
                    if (connected && point > 0)
                        dc.DrawLine(previousX, previousY, x, y);
                    if (!connected)
                        drawX(dc, x, y, xScale, yScale);
                }
            }
        }
    }

    void Plot2DPanel::onIdle(wxIdleEvent& evt)
    {
        if (dataChanged) Refresh();
    }
}
