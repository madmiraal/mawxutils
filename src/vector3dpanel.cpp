// (c) 2014 - 2017: Marcel Admiraal

#include "vector3dpanel.h"

#include <wx/dcbuffer.h>

namespace ma
{
    Vector3DPanel::Vector3DPanel(wxWindow* parent, double maxLength,
            bool useLeft) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
            wxDefaultSize, wxFULL_REPAINT_ON_RESIZE), maxLength(maxLength),
        useLeft(useLeft)
    {
        Bind(wxEVT_ERASE_BACKGROUND, &Vector3DPanel::stopErase, this);
        Bind(wxEVT_PAINT, &Vector3DPanel::paint, this);
        Bind(wxEVT_IDLE, &Vector3DPanel::onIdle, this);
    }

    Vector3DPanel::~Vector3DPanel()
    {
    }

    void Vector3DPanel::setVector(const ma::Vector3D& vector)
    {
        wxMutexLocker lock(dataLock);
        this->vector = vector;
    }

    void Vector3DPanel::setVector(const double* vector)
    {
        setVector(ma::Vector3D(vector[0],vector[1],vector[2]));
    }

    void Vector3DPanel::stopErase(wxEraseEvent& event)
    {
        // Intercepting erase event to stop erasing.
    }

    void Vector3DPanel::paint(wxPaintEvent& event)
    {
        // Update panel coordinates.
        wxSize windowSize = this->GetSize();
        int width = windowSize.GetWidth();
        int height = windowSize.GetHeight();
        xCentre = width / 2;
        yCentre = height / 2;
        scale = width / 2 / maxLength * 0.9;

        // Create paint event device context.
        wxBitmap buffer(windowSize);
        wxBufferedPaintDC dc(this, buffer);
        // Erase background.
        dc.SetBrush(*wxBLACK_BRUSH);
        dc.DrawRectangle(0, 0, width, height);
        // Draw depth scale rings.
        dc.SetPen(*wxWHITE_PEN);
        dc.SetBrush(*wxTRANSPARENT_BRUSH);
        for (unsigned int i = 4; i > 0; i/=2)
        {
            double radius = maxLength * scale / i;
            dc.DrawEllipse(
                xCentre-radius, yCentre-radius/8,
                radius*2, radius*2/8);
        }

        int x, y, z;
        {
            double yOffset = - vector[1] * scale / 8;
            if (useLeft) yOffset *= -1;
            wxMutexLocker lock(dataLock);
            x = xCentre + vector[0] * scale;
            y = yCentre + yOffset;
            z = yCentre - vector[2] * scale + yOffset;
        }
        dc.SetPen(*wxGREEN_PEN);
        dc.DrawLine(xCentre, yCentre, x, z);
        dc.SetPen(*wxYELLOW_PEN);
        dc.DrawLine(x, z, x, y);
        dc.SetTextForeground(*wxWHITE);
        dc.DrawText(wxT("X"), width * 0.95, yCentre);
        double yOffset = - maxLength * scale / 8;
        if (useLeft) yOffset *= -1;
        else yOffset -= 17;
        dc.DrawText(wxT("Y"), xCentre, yCentre + yOffset);
        dc.DrawText(wxT("Z"), xCentre, 0);
    }

    void Vector3DPanel::onIdle(wxIdleEvent& evt)
    {
        Refresh();
    }
}
