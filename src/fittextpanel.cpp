// (c) 2016: Marcel Admiraal

#include "fittextpanel.h"

#include <wx/dcbuffer.h>

namespace ma
{
    FitTextPanel::FitTextPanel(wxWindow* parent, const wxString& text,
        const wxColour& textColour, const wxColour& backgroundColour) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxFULL_REPAINT_ON_RESIZE),
        text(text), textColour(textColour), backgroundColour(backgroundColour)
    {
        Bind(wxEVT_ERASE_BACKGROUND, &FitTextPanel::stopErase, this);
        Bind(wxEVT_PAINT, &FitTextPanel::paint, this);
    }

    void FitTextPanel::setText(const wxString& text)
    {
        this->text = text;
    }

    void FitTextPanel::setTextColour(const wxColor& textColour)
    {
        this->textColour = textColour;
    }

    void FitTextPanel::setBackgroundColour(const wxColor& backgroundColour)
    {
        this->backgroundColour = backgroundColour;
    }

    void FitTextPanel::stopErase(wxEraseEvent& event)
    {
        // Intercepting erase event to stop erasing.
    }

    void FitTextPanel::paint(wxPaintEvent& event)
    {
        // Create buffered paint device context.
        wxSize panelSize = GetSize();
        unsigned int panelWidth = panelSize.GetWidth();
        unsigned int panelHeight = panelSize.GetHeight();
        wxBitmap buffer(panelSize);
        wxBufferedPaintDC dc(this, buffer);
        dc.SetBrush(backgroundColour);
        dc.DrawRectangle(panelSize);

        dc.SetTextForeground(textColour);
        unsigned int pointSize = ma::getBestFitFontSize(
                this, text, panelWidth, panelHeight);
        wxFont font = dc.GetFont();
        font.SetPointSize(pointSize);
        dc.SetFont(font);
        wxSize textSize = dc.GetTextExtent(text);
        dc.DrawText(text,
                (panelWidth - textSize.GetWidth()) / 2,
                (panelHeight - textSize.GetHeight()) / 2);
    }

    unsigned int getBestFitFontSize(const wxWindow* window, const wxString& text,
            const unsigned int width, const unsigned int height)
    {
        unsigned int textWidth = 0;
        unsigned int textHeight = 0;
        wxFont font = window->GetFont();
        if (text.length() == 0) return font.GetPointSize();
        wxCoord xCoord, yCoord, descent, externalLeading;
        while (textWidth < width && textHeight < height)
        {
            font.MakeLarger();
            window->GetTextExtent(text, &xCoord, &yCoord,
                    &descent, &externalLeading, &font);
            textWidth = (unsigned int)xCoord;
            textHeight = (unsigned int)yCoord;
        }
        while (font.GetPointSize() > 3 &&   // MakeSmaller cannot reduce below 3
                (textWidth > width || textHeight > height))
        {
            font.MakeSmaller();
            window->GetTextExtent(text, &xCoord, &yCoord,
                    &descent, &externalLeading, &font);
            textWidth = (unsigned int)xCoord;
            textHeight = (unsigned int)yCoord;
        }
        while (textWidth > width || textHeight > height)
        {
            unsigned int currentSize = font.GetPointSize();
            font.SetPointSize(--currentSize);
            window->GetTextExtent(text, &xCoord, &yCoord,
                    &descent, &externalLeading, &font);
            textWidth = (unsigned int)xCoord;
            textHeight = (unsigned int)yCoord;
        }
        return font.GetPointSize();
    }
}
