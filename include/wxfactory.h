// Utility functions.
//
// (c) 2015 - 2017: Marcel Admiraal

#ifndef WXFACTORY_H
#define WXFACTORY_H

#include <wx/bitmap.h>
#include <wx/colour.h>
#include <wx/window.h>

namespace ma
{
    /**
     * Converts an array of RGBA bytes into a wxBitmap.
     *
     * @param width     The width of the bitmap.
     * @param height    The height of the bitmap.
     * @param rgba      The byte array of RGBA values.
     * @return          The bitmap.
     */
    wxBitmap rgbaTowxBitmap(const unsigned int width, const unsigned int height,
            const unsigned char* rgba);

    /**
     * Populates the colour array with the number of colours required:
     * A total of 19 differnet colours are generated.
     * First 7 are: green, red, yellow, blue, cyan, magenta and white or black
     * depending on whether or not a dark background is being used.
     *
     * @param colourArray   The array of colours to be populated.
     * @param colours       The number of colours required (the array length).
     * @param darkBack      Whether or not the background is dark. Default true.
     */
    void getColours(wxColour colourArray[], const unsigned int colours,
            const bool darkBack = true);
}

#endif // WXFACTORY_H
