// (c) 2015: Marcel Admiraal

#ifndef SERIALPANEL_H
#define SERIALPANEL_H

#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/statbmp.h>

#include "wxfactory.h"
#include "serial.h"
#include "icons.h"

namespace ma
{
    class SerialPanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the parent window.
         * @param serial    The serial port to use.
         */
        SerialPanel(wxWindow* parent, Serial* serial);

        /**
         * Default destructor.
         */
        virtual ~SerialPanel();

        /**
         * Returns the current status.
         *
         * @return The current status.
         */
        wxString getStatus();

        /**
         * Returns the current serial port name.
         *
         * @return The current serial port name.
         */
        wxString getPortName();

        /**
         * Sets the serial port to use.
         *
         * @param serial    The serial port to use.
         */
        void setSerial(Serial* serial);

    private:
        void portNameChanged(wxCommandEvent & event);
        void connectPort(wxCommandEvent &event);
        void setLight();

        Serial* serial;
        wxTextCtrl* portTextCtrl;
        wxButton* connectPortButton;
        wxStaticBitmap* connectedLight;
        wxString status;
    };
}

#endif //SERIALPANEL_H
