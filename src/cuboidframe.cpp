// (c) 2016 - 2017: Marcel Admiraal

#include "cuboidframe.h"

#include <wx/sizer.h>

namespace ma
{
    CuboidFrame::CuboidFrame(wxWindow* parent, wxWindowID id,
            const wxString& title, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name) :
        wxFrame(parent, id, title, pos, size, style, name)
    {
        wxBoxSizer* frameSizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* valueSizer = new wxBoxSizer(wxHORIZONTAL);
        quaternionText = new wxStaticText(this, wxID_ANY, wxT("Quaternion: "));
        valueSizer->Add(quaternionText, 1, wxEXPAND);
        frameSizer->Add(valueSizer, 0, wxEXPAND);

        cuboidPanel = new ma::CuboidPanel(this);
        frameSizer->Add(cuboidPanel, 1, wxEXPAND);
        SetSizer(frameSizer);

        time(&lastRefresh);
        --lastRefresh;
    }

    CuboidFrame::~CuboidFrame()
    {
    }

    void CuboidFrame::setOrientation(const ma::Quaternion& orientation)
    {
        cuboidPanel->setOrientation(orientation);
        time_t now;
        if (difftime(time(&now), lastRefresh) >= 1)
        {
            lastRefresh = now;
            wxString quaternionString("Quaternion: ");
            quaternionString << "[" << orientation.getW() << ", "
                    << orientation.getX() << ", "
                    << orientation.getY() << ", "
                    << orientation.getZ() << "]";
            quaternionText->GetEventHandler()->CallAfter(
                    &wxStaticText::SetLabel, quaternionString);
        }
    }
}
