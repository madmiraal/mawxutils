// A simple wxPanel for displaying two dimensional data.
//
// (c) 2017: Marcel Admiraal

#ifndef PLOT2DPANEL_H
#define PLOT2DPANEL_H

#include <wx/panel.h>
#include <wx/dc.h>
#include <wx/thread.h>

#include "vector.h"

namespace ma
{
    class Plot2DPanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the parent window.
         * @param channels  The number of y data ranges. Default: 1
         * @param xMin      The minimum x value. Default: 0
         * @param xMax      The maximum x value. Default: 1
         * @param yMin      The minimum y value. Default: 0
         * @param yMax      The maximum y value. Default: 1
         * @param axes      Whether or not to display axes. Default: true
         * @param connected Whether or not to connect the data points.
         *                  Default: true
         */
        Plot2DPanel(wxWindow* parent, unsigned int channels = 1,
                double xMin = 0, double xMax = 1,
                double yMin = 0, double yMax = 1,
                bool axes = true, bool connected = true);

        /**
         * Default destructor.
         */
        virtual ~Plot2DPanel();

        /**
         * Sets the x data.
         *
         * @param xData     The new x data.
         */
        void setXData(const Vector& xData);

        /**
         * Sets the y data for a channel.
         *
         * @param yData     The new y data for the specified channel.
         * @param channel   The channel to apply the y data to. Default: 0
         */
        void setYData(const Vector& yData, unsigned int channel = 0);

        /**
         * Sets the x range.
         *
         * @param xMin  The new x minimum.
         * @param xMax  The new x maximum.
         */
        void setXRange(const double xMin, const double xMax);

        /**
         * Sets the y range.
         *
         * @param yMin  The new y minimum.
         * @param yMax  The new y maximum.
         */
        void setYRange(const double yMin, const double yMax);

        /**
         * Sets a channel's colour.
         *
         * @param newColour The new colour to set the channel to.
         * @param channel   Default: 0 - The channel to change the colour of.
         */
        void setColour(wxColour newColour, unsigned int channel = 0);

        /**
         * Sets all the channels' colours.
         *
         * @param newColour The array of colours to set the channels to.
         */
        void setColour(wxColour newColour[]);

        /**
         * Sets whether or not to display the axes.
         *
         * @param axes  Whether or not to display the axes.
         */
        void displayAxes(bool axes = true);

        /**
         * Sets whether or not to connect the data points.
         *
         * @param axes  Whether or not to connect the data points.
         */
        void connectData(bool connected = true);

    private:
        unsigned int channels, dataPoints;
        Vector xData;
        Vector* yData;
        double xMin, xMax, yMin, yMax;
        bool axes, connected;
        wxColor* colour;
        wxMutex dataLock;
        bool dataChanged;

        // Set the number of data points based on the minimum data.
        void setDataPoints();
        // Draw an X at the location and scale specified.
        void drawX(wxDC& dc, double x, double y, double xScale, double yScale);

        // Erase event function.
        void stopErase(wxEraseEvent& event);
        // Paint event function.
        void paint(wxPaintEvent& event);
        // Idle event function.
        void onIdle(wxIdleEvent& evt);
    };
}

#endif // PLOT2DPANEL_H
