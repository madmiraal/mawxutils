// (c) 2014 - 2017: Marcel Admiraal

#include "cuboidpanel.h"

#include <wx/dcbuffer.h>

#include "vector3d.h"

namespace ma
{
    CuboidPanel::CuboidPanel(wxWindow* parent, bool useLeft) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
            wxDefaultSize, wxFULL_REPAINT_ON_RESIZE),
        useLeft(useLeft)
    {
        Bind(wxEVT_ERASE_BACKGROUND, &CuboidPanel::stopErase, this);
        Bind(wxEVT_PAINT, &CuboidPanel::paint, this);
        Bind(wxEVT_IDLE, &CuboidPanel::onIdle, this);
    }

    CuboidPanel::~CuboidPanel()
    {
    }

    void CuboidPanel::setOrientation(const Quaternion& orientation)
    {
        wxMutexLocker lock(dataLock);
        this->orientation = orientation;
    }

    void CuboidPanel::stopErase(wxEraseEvent& event)
    {
        // Intercepting erase event to stop erasing.
    }

    void CuboidPanel::paint(wxPaintEvent& event)
    {
        // Create buffered paint event device context.
        wxSize windowSize = this->GetSize();
        int height = windowSize.GetHeight();
        int width = windowSize.GetWidth();
        wxBitmap buffer(windowSize);
        wxBufferedPaintDC dc(this, buffer);
        dc.SetBrush(*wxBLACK);
        dc.DrawRectangle(0, 0, width, height);

        // Update panel coordinates.
        ma::Vector3D vectorPoint[12];
        vectorPoint[0] = ma::Vector3D(0, 0, 0);     // Centre
        vectorPoint[1] = ma::Vector3D(1, 0, 0);     // Right, centre
        vectorPoint[2] = ma::Vector3D(0, 1, 0);     // Front, centre
        vectorPoint[3] = ma::Vector3D(0, 0, 1);     // Top, centre
        vectorPoint[4] = ma::Vector3D(1, 1, 1);     // Right, front, top
        vectorPoint[5] = ma::Vector3D(1, 1, -1);    // Right, front, bottom
        vectorPoint[6] = ma::Vector3D(1, -1, 1);    // Right, back, top
        vectorPoint[7] = ma::Vector3D(1, -1, -1);   // Right, back, bottom
        vectorPoint[8] = ma::Vector3D(-1, 1, 1);    // Left, front, top
        vectorPoint[9] = ma::Vector3D(-1, 1, -1);   // Left, front, bottom
        vectorPoint[10] = ma::Vector3D(-1, -1, 1);  // Left, back, top
        vectorPoint[11] = ma::Vector3D(-1, -1, -1); // Left, back, bottom
        ma::Quaternion current;
        {
            wxMutexLocker lock(dataLock);
            current = orientation;
        }

        int xCentre = width / 2;
        int yCentre = height / 2;
        int scale = (height > width) ? width / 4 : height / 4;
        int x[12], y[12];
        double zSkew = -0.1;
        if (useLeft) zSkew *= -1;
        for (int point = 0; point < 12; point++)
        {
            vectorPoint[point] = rotate(vectorPoint[point], current);
            double zScale = (vectorPoint[point][1] * zSkew) + 1;
            x[point] = xCentre + vectorPoint[point][0] * scale * zScale;
            y[point] = yCentre - vectorPoint[point][2] * scale * zScale;
        }

        dc.SetPen(wxPen(*wxRED));
        dc.DrawLine(x[0],y[0],x[1],y[1]);
        dc.DrawText(wxT("X"), x[1], y[1]);
        dc.SetPen(wxPen(*wxGREEN));
        dc.DrawLine(x[0],y[0],x[2],y[2]);
        dc.DrawText(wxT("Y"), x[2], y[2]);
        dc.SetPen(wxPen(*wxBLUE));
        dc.DrawLine(x[0],y[0],x[3],y[3]);
        dc.DrawText(wxT("Z"), x[3], y[3]);
        dc.SetPen(wxPen(*wxWHITE));
        dc.DrawLine(x[4],y[4],x[5],y[5]);
        dc.DrawLine(x[4],y[4],x[6],y[6]);
        dc.DrawLine(x[4],y[4],x[8],y[8]);
        dc.DrawLine(x[5],y[5],x[7],y[7]);
        dc.DrawLine(x[5],y[5],x[9],y[9]);
        dc.DrawLine(x[6],y[6],x[7],y[7]);
        dc.DrawLine(x[6],y[6],x[10],y[10]);
        dc.DrawLine(x[7],y[7],x[11],y[11]);
        dc.DrawLine(x[8],y[8],x[9],y[9]);
        dc.DrawLine(x[8],y[8],x[10],y[10]);
        dc.DrawLine(x[9],y[9],x[11],y[11]);
        dc.DrawLine(x[10],y[10],x[11],y[11]);
    }

    void CuboidPanel::onIdle(wxIdleEvent& evt)
    {
        Refresh();
    }
}
