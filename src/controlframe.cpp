// (c) 2016: Marcel Admiraal

#include "controlframe.h"

namespace ma
{
    ControlFrame::ControlFrame(wxWindow* parent, const wxWindowID id,
            const Str& title, const wxPoint& position, const wxSize& size,
            const long style, const wxString& name) :
        ControlFrame(new Control(), parent, id, title, position, size, style,
                name)
    {
    }

    ControlFrame::ControlFrame(Control* control, wxWindow* parent,
            const wxWindowID id, const Str& title, const wxPoint& position,
            const wxSize& size, const long style, const wxString& name) :
        wxFrame(parent, id, wxString(title.c_str()), position, size, style,
                name), control(control), menuBar(new wxMenuBar())
    {
        SetMenuBar(menuBar);
        toolBar = CreateToolBar();
        statusBar = CreateStatusBar();

        Bind(wxEVT_IDLE, &ControlFrame::onIdle, this);
        Bind(wxEVT_CLOSE_WINDOW, &ControlFrame::onClose, this);
    }

    ControlFrame::~ControlFrame()
    {
        delete control;
    }

    void ControlFrame::onIdle(wxIdleEvent& evt)
    {
        // Get status message from the controller.
        updateStatusBar(control->getStatusMessage());
    }

    void ControlFrame::onClose(wxCloseEvent& event)
    {
        updateStatusBar("Exiting...");
        Destroy();
    }

    void ControlFrame::updateStatusBar(const Str& status)
    {
        // Send statusBar->SetStatusText(status) message to primary thread.
        statusBar->GetEventHandler()->CallAfter(
            &wxStatusBar::SetStatusText, wxString(status.c_str()), 0);
    }
}
