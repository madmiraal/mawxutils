// (c) 2017: Marcel Admiraal

#include "texteditordialog.h"

#include <wx/sizer.h>
#include <wx/msgdlg.h>

namespace ma
{
    TextEditorDialog::TextEditorDialog(wxWindow* parent, wxWindowID id,
            const Str& title, const Str& initialText, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name) :
        wxDialog(parent, id, wxString(title.c_str()), pos, size, style, name)
    {
        wxSizer* layout = new wxBoxSizer(wxVERTICAL);
        textCtrl = new wxTextCtrl(this, ID_TEXT, wxString(initialText.c_str()),
                wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxHSCROLL);
        layout->Add(textCtrl, 1, wxEXPAND);
        layout->Add(CreateButtonSizer(wxOK | wxCANCEL), 0, wxEXPAND);
        SetSizer(layout);

        Bind(wxEVT_TEXT_MAXLEN, &TextEditorDialog::maxLengthReached, this);
    }

    unsigned int TextEditorDialog::getLines() const
    {
        return textCtrl->GetNumberOfLines();
    }

    Str TextEditorDialog::getLine(const unsigned int lineNumber) const
    {
        return Str(textCtrl->GetLineText(lineNumber).c_str());
    }

    Str TextEditorDialog::getText() const
    {
        Str text;
        unsigned int lines = getLines();
        for (unsigned int line = 0; line < lines; ++line)
            text += getLine(line) + "\n";
        return text;
    }

    void TextEditorDialog::maxLengthReached(wxCommandEvent& event)
    {
        wxMessageBox(wxT("Unfortunately this simple editor has reached"
            "the maximum length it can handle."));
    }
}
