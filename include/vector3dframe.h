// A wxFrame to display a 3D vector panel.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef VECTOR3DFRAME_H
#define VECTOR3DFRAME_H

#include <wx/frame.h>
#include <wx/stattext.h>

#include "vector3dpanel.h"
#include "vector3d.h"

#include <ctime>

namespace ma
{
    class Vector3DFrame : public wxFrame
    {
    public:
        /**
         * Default constructor. All parameters are optional.
         *
         * @param maxValue  The maximum expected vector length.
         * @param parent    Pointer to the parent window.
         * @param id        The frame's id.
         * @param title     The text displayed at the top of the frame.
         * @param pos       A wxPoint specifying the location of the window.
         * @param size      A wxSize specifying the size of the window.
         * @param style     The combination of wxstyle codes.
         * @param name      A name for the window.
         */
        Vector3DFrame(const double maxLength = 1, wxWindow* parent = 0L,
                wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxDEFAULT_FRAME_STYLE,
                const wxString& name = wxFrameNameStr);

        /**
         * Default destructor.
         */
         virtual ~Vector3DFrame();

        /**
         * Sets the vector to display.
         *
         * @param newVector The 3D vector.
         */
        void setVector(const ma::Vector3D& newVector);

    private:
        ma::Vector3DPanel* vectorPanel;
        wxStaticText* vectorText;
        wxStaticText* magnitudeText;
        time_t lastRefresh;
    };
}

#endif // VECTOR3DFRAME_H
