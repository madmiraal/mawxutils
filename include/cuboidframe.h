// A wxFrame to display a 3D cube panel.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef CUBOIDFRAME_H
#define CUBOIDFRAME_H

#include <wx/frame.h>
#include <wx/stattext.h>

#include "cuboidpanel.h"
#include "quaternion.h"

#include <ctime>

namespace ma
{
    class CuboidFrame : public wxFrame
    {
    public:
        /**
         * Default constructor. All parameters are optional.
         *
         * @param parent    Pointer to the parent window.
         * @param id        The frame's id.
         * @param title     The text displayed at the top of the frame.
         * @param pos       A wxPoint specifying the location of the window.
         * @param size      A wxSize specifying the size of the window.
         * @param style     The combination of wxstyle codes.
         * @param name      A name for the window.
         */
        CuboidFrame(wxWindow* parent = 0L, wxWindowID id = wxID_ANY,
                const wxString& title = wxEmptyString,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxDEFAULT_FRAME_STYLE,
                const wxString& name = wxFrameNameStr);

        /**
         * Default destructor.
         */
         virtual ~CuboidFrame();

        /**
         * Sets the orientation of the cube.
         *
         * @param orientation The rotation quaternion describing
         *                    the orientation of the cube.
         */
        void setOrientation(const ma::Quaternion& orientation);

    private:
        ma::CuboidPanel* cuboidPanel;
        wxStaticText* quaternionText;
        time_t lastRefresh;
    };
}

#endif // CUBOIDFRAME_H
