// A simple wxPanel oscilloscope.
//
// (c) 2014 - 2017: Marcel Admiraal

#ifndef OSCILLOSCOPEPANEL_H
#define OSCILLOSCOPEPANEL_H

#include <wx/panel.h>
#include <wx/colour.h>

#include "temporaldata.h"
#include "vector.h"

namespace ma
{
    class OscilloscopePanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the parent window.
         * @param channels  The number of input channels. Default: 1
         * @param display   The milliseconds to display. Default: 1000
         * @param dataMin   The minimum value of the data. Default: -1
         * @param dataMax   The maximum value of the data. Default: 1
         * @param marker    The marker peropd in milliseconds. Default: 1000
         * @param darkBack  Whether to use a white background. Default true.
         */
        OscilloscopePanel(wxWindow* parent, const unsigned int channels = 1,
                const unsigned int displayPeriod = 1000,
                const double dataMin = -1, const double dataMax = 1,
                const unsigned int markerPeriod = 1000,
                const bool darkBack = true);

        /**
         * Default destructor.
         */
        virtual ~OscilloscopePanel();

        /**
         * Adds data point to a channel.
         *
         * @param value   The data value to add to the channel.
         * @param channel The channel to add the data to. Default: 0
         */
        void addData(const double value, const unsigned int channel = 0);

        /**
         * Adds data points to all channels.
         *
         * @param value The vector with the data values to add to the channels.
         */
        void addData(const Vector& value);

        /**
         * Sets a channel's colour.
         *
         * @param newColour The new colour to set the channel to.
         * @param channel   Default: 0 - The channel to change the colour of.
         */
        void setColour(const wxColour& newColour,
                const unsigned int channel = 0);

        /**
         * Sets all the channels' colours.
         *
         * @param newColour The array of colours to set the channels to.
         */
        void setColour(const wxColour newColour[]);

        /**
         * Freezes or unfreezes the output.
         *
         * @param freeze    Whether to freeze (true) or unfreeze (false) the
         *                  output. Default: true.
         */
        void freezeOutput(const bool freeze = true);

    private:
        TemporalData data;
        double dataMin, dataMax;
        unsigned int displayPeriod, markerPeriod;
        wxColor* colour;
        bool darkBack;
        bool frozen;

        // Erase event function.
        void stopErase(wxEraseEvent& event);
        // Paint event function.
        void paint(wxPaintEvent& event);
        // Timer event function.
        void onIdle(wxIdleEvent& evt);
    };
}

#endif // OSCILLOSCOPEPANEL_H
